# RASA NLU meets PHP

This repository is the outcome of my talk "Natural Language Understanding meets PHP" at 2018's PHP Central Europe conference.

*Included:*

* Docker environment to start
* * Rasa NLU HTTP API
* * PHP client application in a container
* slides in markdown with all notes (simply run them in reveal.js to see them nice)

# Usage

To start both app run `docker-compose up -d` inside the `expamples/` folder:

```bash

$ cd examples/
$ docker-compose up -d

```

Then enter the app container to start working against RASA HTTP API with PHP:

```bash
$ docker exec -it rasa-nlu-client sh
$ cd /app/src/ # Todo: make a Working dir
$ bin/console

```

You should se an image like:
![Console Overview - RASA NLU Commands](console_list.jpg)

*Disclaimer*:
The command are looking like a coupling into a symfony appplication. And indeed that example got a symfony console for a faster presentation.
But under the hood a library is born, which can be introduced in any PHP application at the end. You should have a look into the `rasa_client/lib/` folder to get an overview.

# Getting started

To start a new project including configuration for intents you can use one of the prepared examples in `rasa_client/data/` and run:

```bash
$ bin/console rasa:nlu:train --project=<your-project-name> data/config_<select-one-you-like>.yml
```

To see the result run:

```bash
$ bin/console rasa:nlu:status
```

The output should show your new model you can start using now:

```bash
$ bin/console rasa:nlu:parse --project=<your-project-name> "<text-to-parse>"
```

If the result does not match your expectation, you should move on training.
