## Natural language understanding
### PHP Central Europe - Prague 2018
#### Maximilian Berghoff

---

<span class="title-container">![Titel Bild](https://avatars2.githubusercontent.com/u/2905834?s=400&v=4)</span>
#### Maximilian Berghoff <!-- .element: class="fragment" -->

---
# NLU
## Natural language understanding<!-- .element: class="fragment" -->

Note: to fill the slide, it is getting bigger now
- NLU is the short syntax
- do not mix it up with ... (CLICK) ...

---

# NLP
## Natural Language Processing <!-- .element: class="fragment" -->
# NLU != NLP<!-- .element: class="fragment" -->

Note: ..
- NLP and NLU got something in common but 
- NLP is more i.e. it takes care on processing datasets of  unstructured language data
- but, what is NLU?

---

# NLU

* directly enables interaction of an human with an machine
* brings structure into the spoken/written word
* AI aggregates data

---

# Intent
- what could be the pupose of the text
- what the wants to tell us
- depends on domain/model you are working

---

# Entity

- usefull data for the intent
- i.e. when asking for a topic the topic itself can be the entity

Note: ..
- but it will be better, with an example

---

# NLU - Example

> I have to stay home until friday.

Note: ...
- lets have a look into an example
- to understand that, we do have to train our AI equal or less equal sentences, where you know the intent
- and then ... (CLICK) ...

---

# NLU - Example

> I have to stay home until <span class="mark">friday</span>. 

* Intent: `report illness at work`
* Entity: `end_date=friday`

Note: ...
- due to some magic, we get an intent and an entity.
- depending on your use case
- intent: can be: Request/Report for illness in company context.
- entity:
- - depending on training, you can also define entities
- - AI is responsible find a match for a timing here

---

# NLU - Why?

- Chat bot
- Interface for our machine
- Intent analysis
- for fun

Note: ...
- ...
- but wait a minute ... (CLICK) ...

---

# What about?

## Alexa<!-- .element: class="fragment" -->
## Siri<!-- .element: class="fragment" -->
## Google Home<!-- .element: class="fragment" -->

Note: ..
- it seems, there still are solutions available
- just write a skill, for you use case
- it is getting better, but some month ago i would just need to say  ... (CLICK) ...

---

# Data Privacy ?

Note: ...
- does anybody of you know where your data goes, when asking alexa?
- do we have to care about that?
- yes, i did not looked for it, but anybody seen an data policy, which explains where user data goes
when talking with alexa and co.
- i do not want to
- ...(CLICK) ...

----

# DIY

Note:
- So what about a solution, you can run on your own
- you do have the control
- you should care about hardware, indeed, but maybe a Alexa solution would not have been to big
- so, you can train your model the way you need it.

---

# What to use?
## RASA NLU<!-- .element: class="fragment" -->

Note: ...
- sure there are more, but i get in contact with Rasa this sommer
- ...(CLICK) ...^

---

# RASA NLU

- ready to use
- recognize intents
- extracts entity values
- conversation

---

# Install & RUN RASA NLU

```bash
pip install rasa_nlu
# OR
# git clone https://github.com/RasaHQ/rasa_nlu.git 
cd rasa_nlu
pip install -r requirements.txt
pip install -e .
```

Note: ...
- simply use pip to install it,
- you can also use the latests git repo and do it on your own
- but wait, ... What is that?

---

# Python ???

Note: ..
- Wtf we are at PHP_CE
- Who is able to write its code in python here?
- Really?
- but how to integrate into a PHP-Application?

---

# Implementation in PHP

Note: ...
- nooo, ..
- there is a good reason lots of the AI stuff is running on python
- we do not want to simply copy all the goodies from others

---

# Decorator on RASA NLU API

Note: ...
- but what we can do is: write a kind of a decorator for the API of RASA NLU
- and this is what i did
- d... last night ...
- it is just a dummy to explain common usecases
- so lets go into the console

---

# Into Console/IDE

<span class="title-container">![Console Commands](./console_list.jpg)</span>

---

# Questions

- Ask Now!
- Twitter: @ElectricMaxxx <!-- .element: class="fragment" -->
- Mail: Maximilian.Berghoff@mayflower.de <!-- .element: class="fragment" -->

---

# Links

- [Examples and Slides (md)](https://gitlab.com/ElectricMaxxx/php-meets-nlu)
- [RASA Documentation](https://rasa.com/docs/getting-started/overview/)

---

# Thank You!
